import flask

from srv import babel


AVAILABLE_LANGUAGES = ['ne','es']

@babel.localeselector
def get_locale():
    return flask.request.accept_languages.best_match(AVAILABLE_LANGUAGES)