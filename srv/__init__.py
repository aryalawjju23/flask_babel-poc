import flask 
from flask_babel import Babel
app = flask.Flask(__name__)
babel = Babel(app)

import srv.localization

@app.route('/')
def homerun():
    return flask.render_template(
        '/index.html'
    )
